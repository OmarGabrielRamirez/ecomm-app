import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecom_app/screens/products_by_category_screen.dart';
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

class HomeProductCategory extends StatefulWidget {
  final int categoryId;
  final String urlImage;
  final String categoryName;
  HomeProductCategory(this.urlImage, this.categoryName, this.categoryId);

  @override
  _HomeProductCategoryState createState() => _HomeProductCategoryState();
}

class _HomeProductCategoryState extends State<HomeProductCategory> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 140.0,
      height: 190.0,
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProductsByCategoryScreen(
                      categoryName: widget.categoryName,
                      categoryId: widget.categoryId)));
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          elevation: 5,
          margin: EdgeInsets.all(10),
          child: Stack(children: <Widget>[
            Container(
              child: ClipRRect(
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  imageUrl: widget.urlImage,
                  placeholder: (context, url) => SizedBox(
                    child: Center(
                      child: LoadingIndicator(
                        indicatorType: Indicator.circleStrokeSpin,
                        color: Colors.red,
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 10, bottom: 10, right: 10),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  '${widget.categoryName}',
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontFamily: 'Lato',
                      // fontWeight: FontWeight.bold,
                      letterSpacing: 1.0),
                  // minFontSize: 10,
                  textAlign: TextAlign.left,
                  // stepGranularity: 10,
                  maxLines: 3,
                ),
              ),
            )
          ]),
        ),
      ),
    );
  }
}
