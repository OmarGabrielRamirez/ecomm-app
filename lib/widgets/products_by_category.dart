import 'package:ecom_app/models/product.dart';
import 'package:ecom_app/screens/product_detail.dart';
import 'package:flutter/material.dart';

class ProductByCategory extends StatefulWidget {

  final Product product;
  ProductByCategory(this.product);

  @override
  _ProductByCategoryState createState() => _ProductByCategoryState();
}

class _ProductByCategoryState extends State<ProductByCategory> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.0,
      width: 190.0,
      child: InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetailScreen(this.widget.product)));
        },
              child: Card(
          child: Column(children: <Widget>[
            Text(this.widget.product.name),
            Image.network(widget.product.photo, width: 140.0, height: 140.0),
            Text('Price: ${this.widget.product.price}'),
            Text('Discount: ${this.widget.product.discount}'),
          ],
        ),
      ),
    )
      
  );
}
}