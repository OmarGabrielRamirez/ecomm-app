import 'package:ecom_app/models/user.dart';
import 'package:ecom_app/models/user_register.dart';
import 'package:ecom_app/repository/repository.dart';

class UserService {
  Repository _repository;

  UserService() {
    _repository = Repository();
  }

  createUser(UserRegister user) async {
    return await _repository.httpPost(
      'customer/register',
      user.toJson(),
    );
  }

  login(User user) async {
    return await _repository.httpPost(
      'customer/login',
      user.toJson(),
    );
  }
}
