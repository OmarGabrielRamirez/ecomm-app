import 'package:ecom_app/repository/repository.dart';
import 'package:http/http.dart' as http;

class ProductService{

  Repository _repository; 
  ProductService(){
    _repository = Repository();
  }

  getHotProducts() async {
       return await http.get('http://touch-connect.online/api/products?limit=10&page=1');
  }

  getAllnewArrivalsProducts() async {
        return await http.get('http://touch-connect.online/api/products?limit=10&page=1');
  }
  getProductsByCategoryId(categoryId) async{
    return await _repository.httpGetById('get-products-by-category', categoryId);
  }

  getAllProducts() async {
    return await _repository.httpGet("products");
  }
  
}