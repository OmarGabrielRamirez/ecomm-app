import 'package:ecom_app/models/user_sesion.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> saveSessionUser(
    UserSession usersesion, BuildContext context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  _prefs.setInt('userId', usersesion.idUser);
  _prefs.setString('userName', usersesion.name);
  _prefs.setString('userEmail', usersesion.email);
  _prefs.setBool('userInvited', false);

  if (_prefs.getString('userName').isNotEmpty &&
      _prefs.getString('userEmail').isNotEmpty &&
      _prefs.getInt('userId').toString().isNotEmpty) {
    return true;
  } else {
    return false;
  }
}
