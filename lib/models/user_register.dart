class UserRegister {
  String name;
  String lastName;
  String email;
  String password;
  String passwordConfirm;

  toJson() {
    return {
      'first_name': name,
      'last_name': lastName,
      'email': email,
      'password': password,
      'password_confirmation': passwordConfirm
    };
  }
}
