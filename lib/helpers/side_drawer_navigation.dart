import 'package:ecom_app/screens/address_page.dart';
import 'package:ecom_app/screens/order_list_screen.dart';
import 'package:flutter/material.dart';

import '../screens/login_screen.dart';

class SideDrawerNavigation extends StatefulWidget {
  SideDrawerNavigation({Key key}) : super(key: key);

  @override
  _SideDrawerNavigationState createState() => _SideDrawerNavigationState();
}

class _SideDrawerNavigationState extends State<SideDrawerNavigation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.redAccent,
      child: Drawer(
        child: Container(
          color: Colors.redAccent,
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(
                decoration: BoxDecoration(color: Colors.redAccent),
                accountName: Text('enelviaje'),
                accountEmail: Text('enelviaje7@gmail.com'),
                currentAccountPicture: GestureDetector(
                  child: CircleAvatar(
                    radius: 50,
                    child: Image.asset('assets/user.png'),
                  ),
                ),
              ),
              ListTile(
                title: Text(
                  'Home',
                  style: TextStyle(color: Colors.white),
                ),
                leading: Icon(Icons.home, color: Colors.white),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddressPage(),
                    ),
                  );
                },
                child: ListTile(
                  title: Text(
                    'Address',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(Icons.place, color: Colors.white),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OrderListScreen(),
                    ),
                  );
                },
                child: ListTile(
                  title: Text(
                    'Order List',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(Icons.list, color: Colors.white),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginScreen(),
                    ),
                  );
                },
                child: ListTile(
                  title: Text(
                    'Salir',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(Icons.exit_to_app, color: Colors.white),
                ),
              ),
              // ListTile(
              //   title: Text('Home'),
              //   leading: Icon(Icons.home),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
