import 'dart:convert';

import 'package:ecom_app/models/product.dart';
import 'package:ecom_app/services/product_services.dart';
import 'package:ecom_app/widgets/products_by_category.dart';
import 'package:flutter/material.dart';

class ProductsByCategoryScreen extends StatefulWidget {

  

  final String categoryName; 
  final int categoryId;

   ProductsByCategoryScreen({this.categoryName, this.categoryId});
  
   
  @override
  _ProductsByCategoryScreenState createState() => _ProductsByCategoryScreenState();
}

class _ProductsByCategoryScreenState extends State<ProductsByCategoryScreen> {

  ProductService _productService = ProductService();
  List<Product> _productListByCategory = List<Product>();

  _getProductsByCategory() async {
    var products = await _productService.getProductsByCategoryId(this.widget.categoryId);
    var _list = json.decode(products.body);
    _list['data'].forEach((data){
      var model = Product();
      model.id = data['id'];
      model.name = data['name'];
      model.photo = data['photo'];
      model.price = data['price'];
      model.discount = data['discount'];
      model.productDetail = data['detail'];

      setState(() {
        _productListByCategory.add(model);

      });
     });
  }


  @override
  void initState() {
     super.initState();
    _getProductsByCategory();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: 
      AppBar(
        title: Text(this.widget.categoryName),
      ),

      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: GridView.builder(
            
            //Implementamos Columnas
            gridDelegate:  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            //Mostramos Todos los elementos de la lista segun el id de la categoria
            itemCount: _productListByCategory.length,
            //Pasamos los parametros para mostrar los objetos
            itemBuilder: (context, index){
              return ProductByCategory(this._productListByCategory[index]);

            }
            
            ),
            ),
      )
      
    );
  }

  
}