import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecom_app/delegates/product_search.dart';
import 'package:ecom_app/helpers/side_drawer_navigation.dart';
import 'package:ecom_app/models/category.dart';
import 'package:ecom_app/models/product.dart';
import 'package:ecom_app/providers/cart_provider.dart';
import 'package:ecom_app/screens/cart_screen.dart';
import 'package:ecom_app/services/cart_services.dart';
import 'package:ecom_app/services/category_services.dart';
import 'package:ecom_app/services/product_services.dart';
import 'package:ecom_app/services/slider_services.dart';
import 'package:ecom_app/widgets/home_hot_products.dart';
import 'package:ecom_app/widgets/home_new_products.dart';
import 'package:ecom_app/widgets/home_product_categories.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  SliderService _sliderService = SliderService();
  CategoryService _categoryService = CategoryService();
  ProductService _productService = ProductService();

  List<Category> _categoryList = List<Category>();
  List<Product> _productList = List<Product>();
  List<Product> _allProductList = List<Product>();
  List<Product> _newArrivalProducts = List<Product>();

  CartService _cartService = CartService();
  List<Product> _cartItems;

  var items = [];

  @override
  void initState() {
    super.initState();
    _getAllSliders();
    _getAllCategories();
    _getAllHotProducts();
    _getAllNewArrivalProducts();
    _getCartItems();
    _getAllHotProducts();
  }

  _getCartItems() async {
    _cartItems = List<Product>();
    var cartItems = await _cartService.getCartItems();
    cartItems.forEach((data) {
      var product = Product();
      product.id = data['productId'];
      product.name = data['productName'];
      product.photo = data['productPhoto'];
      product.price = data['productPrice'];
      product.discount = data['productDiscount'];
      product.productDetail = data['productDetail'] ?? 'No detail';
      product.quantity = data['productQuantity'];
      setState(() {
        _cartItems.add(product);
      });
    });
  }

  _getAllSliders() async {
    var sliders = await _sliderService.getSliders();
    var result = json.decode(sliders.body);
    result['data'].forEach((data) {
      setState(() {
        items.add(data['image_url']);
      });
    });
  }

  _getAllCategories() async {
    var categories = await _categoryService.getCategories();
    var result = json.decode(categories.body);
    result['data'].forEach((data) {
      var model = Category();
      model.id = data['id'];
      model.name = data['name'];
      model.url = data['image_url'] == null
          ? 'https://cdn.iconscout.com/icon/premium/png-512-thumb/souvenir-shop-1639703-1391201.png'
          : data['image_url'];
      setState(() {
        _categoryList.add(model);
      });
    });
  }

  _getAllProducts() async {
    var products = await _productService.getAllProducts();
    var result = json.decode(products.body);
    result['data'].forEach((data) {
      var model = Product();
      model.id = data['id'];
      model.name = data['name'];
      model.photo = data['photo'];
      model.price = data['price'];
      model.discount = data['discount'];
      model.productDetail = data['detail'];

      setState(() {
        _allProductList.add(model);
      });
    });
  }

  _getAllHotProducts() async {
    var hotProducts = await _productService.getHotProducts();
    var result = json.decode(hotProducts.body);
    result['data'].forEach((data) {
      print(data['images'][0]['medium_image_url']);
      var model = Product();
      model.id = data['id'];
      model.name = data['name'];
      model.price = 0;
      model.quantity = 100;
      model.photo = data['images'][0]['medium_image_url'];
      model.priceVisual = data['price'];
      model.discount = data['discount'] == null ? 0 : data['discount'];
      model.productDetail = data['description'];
      setState(() {
        _productList.add(model);
      });
    });
  }

  _getAllNewArrivalProducts() async {
    var newProducts = await _productService.getAllnewArrivalsProducts();
    var result = json.decode(newProducts.body);
    result['data'].forEach((data) {
      var model = Product();
      model.id = data['id'];
      model.name = data['name'];
      model.price = 0;
      model.quantity = 100;
      model.photo = data['images'][0]['medium_image_url'];
      model.priceVisual = data['price'];
      model.discount = data['discount'] == null ? 0 : data['discount'];
      model.productDetail = data['description'];
      setState(() {
        _newArrivalProducts.add(model);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final cartProvider = Provider.of<CartProvider>(context);
    cartProvider.cartItems();
    return Scaffold(
      drawer: SideDrawerNavigation(),
      appBar: AppBar(
        title: Text('eComm App'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(
                    context: context,
                    delegate: ProductSearch(products: _allProductList));
              }),
          InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CartScreen(_cartItems)));
            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                height: 150,
                width: 30,
                child: Stack(
                  children: <Widget>[
                    IconButton(
                      iconSize: 30,
                      icon: Icon(
                        Icons.shopping_cart,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                    Positioned(
                      child: Stack(
                        children: <Widget>[
                          Icon(Icons.brightness_1,
                              size: 25, color: Colors.black38),
                          Positioned(
                            top: 4.0,
                            right: 8.0,
                            child: Center(
                              child:
                                  Text(cartProvider.getCartQuantity.toString()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
      body: ContenidosHome(),
    );
  }

  Widget ContenidosHome() {
    return Container(
      child: ListView(
        children: <Widget>[
          Container(
            child: SizedBox(
              height: 250,
              width: MediaQuery.of(context).size.width,
              child: CarouselSlider(
                options: CarouselOptions(
                  viewportFraction: 10.0,
                  aspectRatio: 1.0,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 3),
                  autoPlayAnimationDuration: Duration(milliseconds: 100),
                  autoPlayCurve: Curves.easeInToLinear,
                  enlargeCenterPage: false,
                  scrollDirection: Axis.horizontal,
                ),
                items: items.map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          child: CachedNetworkImage(
                            imageUrl: i,
                            fit: BoxFit.cover,
                          ),
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Categorias'),
          ),
          HomeProductCategories(categoryList: _categoryList),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Hot Products'),
          ),
          HomeHotProducts(productList: _productList),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('New Products'),
          ),
          HomeNewProducts(productNewList: _newArrivalProducts)
        ],
      ),
    );
  }
}
