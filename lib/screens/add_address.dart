import 'package:flutter/material.dart';

class AddAddresPage extends StatefulWidget {
  AddAddresPage({Key key}) : super(key: key);

  @override
  _AddAddresPageState createState() => _AddAddresPageState();
}

class _AddAddresPageState extends State<AddAddresPage> {
  final name = TextEditingController();

  final email = TextEditingController();

  final address = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add address'),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 0.0),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 28.0, right: 28.0, bottom: 14.0),
              child: Text('Add Shipping Address',
                  style:
                      TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold)),
            ),
            Divider(
              height: 5.0,
              color: Colors.black,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                controller: name,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    hintText: 'Enter your address',
                    labelText: 'Enter your address'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                keyboardType: TextInputType.text,
                controller: email,
                decoration: InputDecoration(
                    hintText: 'Enter your city', labelText: 'Enter your city'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                keyboardType: TextInputType.text,
                controller: email,
                decoration: InputDecoration(
                    hintText: 'Enter your postcode',
                    labelText: 'Enter your postcode'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                keyboardType: TextInputType.text,
                controller: email,
                decoration: InputDecoration(
                    hintText: 'Enter your state',
                    labelText: 'Enter your state'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                keyboardType: TextInputType.number,
                controller: email,
                decoration: InputDecoration(
                    hintText: 'Enter your phone',
                    labelText: 'Enter your phone'),
              ),
            ),
            Column(
              children: <Widget>[
                ButtonTheme(
                  minWidth: 320.0,
                  height: 45.0,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7.0)),
                    color: Colors.redAccent,
                    onPressed: () {},
                    child: Text('Add Address',
                        style: TextStyle(color: Colors.white)),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
